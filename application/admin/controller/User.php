<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Session;
use think\Validate;

class User extends Controller
{
    protected $auth_group_model;
    protected $auth_rule_model;

    protected function _initialize()
    {
        parent::_initialize();
        $this->auth_group_model = new AuthGroupModel();
        $this->auth_rule_model  = new AuthRuleModel();
    }
    public function index()
    {
        return $this->fetch();
    }

    public function login()
    {
        $post     = $this->request->post();
        $validate = validate('User');
        $validate->scene('login');
        $user_id = Db::name('user')
            ->where('username', $post['username'])
            ->value('id');
        if (!$validate->check($post)) {
            $this->error($validate->getError());
        } else {
            $sql_password = Db::name('user')
                ->where('username', $post['username'])
                ->value('password');
            if (md5($post['password']) !== $sql_password) {
                $this->error('密码错误');
            } else {
                session('username', $post['username']);
                session('user_id', $user_id);
                Db::name('user')
                    ->where('username',$post['username'])
                    ->update(['last_login_ip'=>$_SERVER['REMOTE_ADDR'],'last_login_time'=>date('Y-m-d h:i:s',time())]);
                $this->success('登陆成功', 'index/index');
            }
        }
    }
    public function logOut()
    {
        session('username', null);
        $this->redirect('admin/user/index');
    }

    public function userlist()
    {
        $data = Db::name('User')
            ->paginate(12);
        $this->assign('users', $data);
        return $this->fetch();
    }
    //打开新增界面
    public function showAdd()
    {
        return $this->fetch('add');
    }
    //增加用户
    public function addUser()
    {
        $post     = $this->request->post();
        $validate = validate('User');
        $res      = $validate->check($post);
        if ($res !== true) {
            $this->error($validate->getError());
        } else {
            unset($post['check_password']);
            $post['password'] = md5($post['password']);
            $post['last_login_ip'] = '0.0.0.0';
            $post['create_time']   = date('Y-m-d h:i:s', time());
            Db::name('user')
                ->insert($post);
            $this->success('success');
        }
    }
    //编辑页面
    public function edit($id)
    {
        $data = Db::name('User')
            ->where('id', $id)
            ->find();
        $this->assign('data', $data);
        return $this->fetch();
    }
    //编辑提交
    public function editUser()
    {
        $post     = $this->request->post();
        $validate = validate('User');
        if (empty($post['password']) && empty($post['check_password'])) {
            $res = $validate->scene('edit')->check($post);
            if ($res !== true) {
                $this->error($validate->getError());
            } else {
                unset($post['password']);
                unset($post['check_password']);
                $db = Db::name('user')
                    ->where('id', $post['id'])
                    ->update(
                        [
                            'username' => $post['username'],
                            'email'    => $post['email'],
                        ]);
                $this->success('编辑成功');
            }
        } else {
            $res = $validate->scene('editPassword')->check($post);
            if ($res !== true) {
                $this->error($validate->getError());
            } else {
                unset($post['check_password']);
                $post['password'] = md5($post['password']);
                $db               = Db::name('user')
                    ->where('id', $post['id'])
                    ->update($post);
                $this->success('编辑成功');
            }
        }
    }
    public function deleteUser()
    {
        $id = $this->request->post('id');
        $username =  Db::name('user')
            ->where('id',$id)
            ->value('username');
        if ((int) $id !== 1) {
            if($username!==session('username')){
                 $db = Db::name('user')
                ->where('id', $id)
                ->delete();
                $this->success('删除成功');
            }else{
                 $this->error('无法删除当前登录用户');
            }
        } else {
            $this->error('超级管理员无法删除');
        }
    }
    function auth(){
        return $this->fetch();
    }
    /**
     * AJAX获取规则数据
     * @param $id
     * @return mixed
     */
    public function getJson($id)
    {
        $auth_group_data = $this->auth_group_model->find($id)->toArray();
        $auth_rules      = explode(',', $auth_group_data['rules']);
        $auth_rule_list  = $this->auth_rule_model->field('id,pid,title')->select();

        foreach ($auth_rule_list as $key => $value) {
            in_array($value['id'], $auth_rules) && $auth_rule_list[$key]['checked'] = true;
        }

        return $auth_rule_list;
    }

}
