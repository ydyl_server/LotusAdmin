<?php
namespace app\admin\controller;

use \think\Db;
use \think\Reuquest;
class auth extends Main
{
    function index(){
        //获取权限列表
        $auth = Db::name('auth_rule')->order(['sort' => 'DESC', 'id' => 'ASC'])->select();
        $auth = array2Level($auth);
        return $this->fetch('index',['auth'=>$auth]);
    }
    function showAdd(){
    	$auth = Db::name('auth_rule')->order(['sort' => 'DESC', 'id' => 'ASC'])->select();
        $auth = array2Level($auth);
    	return  $this->fetch('add',['auth'=>$auth]);
    }
    function add(){
    	$post = $this->request->post();
    	$validate = validate('auth');
    	$res = $validate->check($post);
		if($res!==true){
			$this->error($validate->getError());
		}else{
			Db::name('auth_rule')
			->insert($post);
			$this->success('success');
		}
    }
    function showEdit(){
        $id  = $this->request->get('id');
        $pid = Db::name('auth_rule')
            ->where('id',$id)
            ->value('pid');
        if($pid!==0){
            $p_title = Db::name('auth_rule')
                ->where('id',$pid)
                ->value('title');
        }else{
            $p_title = '顶级菜单';
        }
        $this->assign('p_title',$p_title);
        $data  =   Db::name('auth_rule')
            ->where('id',$id)
            ->find();
        return  $this->fetch('edit',['data'=>$data]);
    }
    function edit(){
        $post =  $this->request->post();
        $id = $post['id'];
        $validate = validate('auth');
        $validate->scene('edit');
        $res = $validate->check($post);
        if($res!==true){
            $this->error($validate->getError());
        }else{
            unset($post['id']);
            Db::name('auth_rule')
            ->where('id',$id)
            ->update($post);
            $this->success('success');            
        }
    }
    function delete(){
        $id = $this->request->post('id');
        $juge = Db::name('auth_rule')
            ->where('pid',$id)
            ->find();
        if($id<4){
                 $this->error('重要节点无法删除'); 
        }
        if(!empty($juge)){ 
                $this->error('请先删除子权限'); 
        }else{

            Db::name('auth_rule')
            ->delete($id);
            $this->success('success');
        }
    
    }
}
